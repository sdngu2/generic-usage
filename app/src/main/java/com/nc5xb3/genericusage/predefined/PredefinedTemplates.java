package com.nc5xb3.genericusage.predefined;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.nc5xb3.genericusage.model.source.Input;
import com.nc5xb3.genericusage.model.source.Template;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Steven Nguyen on 12/3/2015.
 */
public class PredefinedTemplates {

    public static Template getExample() {
        Template example = new Template("_");
        example.setVersion("1.0.0.0");
        example.setLastUpdated(new Date());
        example.setImageUrl("");
        example.setName("Example");
        example.setDescription("Example of a source");
        example.setCategories(new ArrayList<String>(){{add("Example");}});
        List<Input> inputList = new ArrayList<>();

        Input input = new Input();
        input.setName("1");
        input.setLabel("Number");
        input.setType("number");
        input.setValues(new ArrayList<String>(){{add("1");}});
        inputList.add(input);

        input = new Input();
        input.setName("2");
        input.setLabel("Text N");
        input.setType("text");
        input.setValues(getCurrencyList("USD - US Dollar"));
        inputList.add(input);

        input = new Input();
        input.setName("3");
        input.setLabel("Text 1");
        input.setType("text");
        input.setValues(new ArrayList<String>(){{add("");}});
        inputList.add(input);

        input = new Input();
        input.setName("4");
        input.setLabel("Password");
        input.setType("password");
        input.setValues(new ArrayList<String>(){{add("");}});
        inputList.add(input);

        example.setFormInputList(inputList);

        return example;
    }

    public static Template getXE(String id, Context context) {
        Template templateXE = new Template("idXE_" + id);

        templateXE.setVersion("0.1");
        templateXE.setLastUpdated(new Date());
        templateXE.setImageUrl("drawable/source_xe");
        int imageResource = context.getResources().getIdentifier(templateXE.getImageUrl(), null,
                context.getPackageName());
        Bitmap image = BitmapFactory.decodeResource(context.getResources(), imageResource);
        templateXE.setImage(image);
        templateXE.setName("XE_" + id);
        templateXE.setDescription("Currency Exchange Rates");
        templateXE.setCategories(new ArrayList<String>(){{add("Finance");}});
        List<Input> inputListXE = new ArrayList<>();

        Input input = new Input();
        input.setName("amount");
        input.setLabel("Amount");
        input.setType("number");
        input.setValues(new ArrayList<String>(){{add("1");}});
        inputListXE.add(input);

        input = new Input();
        input.setName("from");
        input.setLabel("From");
        input.setType("text");
        input.setValues(getCurrencyList("USD - US Dollar"));
        inputListXE.add(input);

        input = new Input();
        input.setName("to");
        input.setLabel("To");
        input.setType("text");
        input.setValues(getCurrencyList("AUD - Australian Dollar"));
        inputListXE.add(input);

        templateXE.setFormInputList(inputListXE);

        return templateXE;
    }

    private static List<String> getCurrencyList(String defaultString) {
        List<String> list = new ArrayList<>();
        list.add(defaultString);
        list.add("USD - US Dollar");
        list.add("EUR - Euro");
        list.add("GBP - British Pound");
        list.add("INR - Indian Rupee");
        list.add("AUD - Australian Dollar");
        list.add("CAD - Canadian Dollar");
        list.add("SGD - Singapore Dollar");
        list.add("CHF - Swiss Franc");
        list.add("MYR - Malaysian Ringgit");
        list.add("JPY - Japanese Yen");
        list.add("CNY - Chinese Yuan Renminbi");
        list.add("NZD - New Zealand Dollar");
        list.add("THB - Thai Baht");
        list.add("HUF - Hungarian Forint");
        list.add("AED - Emirati Dirham");
        list.add("HKD - Hong Kong Dollar");
        list.add("MXN - Mexican Peso");
        list.add("ZAR - South African Rand");
        list.add("PHP - Philippine Peso");
        list.add("SEK - Swedish Krona");
        list.add("IDR - Indonesian Rupiah");
        list.add("SAR - Saudi Arabian Riyal");
        list.add("BRL - Brazilian Real");
        list.add("TRY - Turkish Lira");
        list.add("KES - Kenyan Shilling");
        list.add("KRW - South Korean Won");
        list.add("EGP - Egyptian Pound");
        list.add("IQD - Iraqi Dinar");
        list.add("NOK - Norwegian Krone");
        list.add("KWD - Kuwaiti Dinar");
        list.add("RUB - Russian Ruble");
        list.add("DKK - Danish Krone");
        list.add("PKR - Pakistani Rupee");
        list.add("ILS - Israeli Shekel");
        list.add("PLN - Polish Zloty");
        list.add("QAR - Qatari Riyal");
        list.add("XAU - Gold Ounce");
        list.add("OMR - Omani Rial");
        list.add("COP - Colombian Peso");
        list.add("CLP - Chilean Peso");
        list.add("TWD - Taiwan New Dollar");
        list.add("ARS - Argentine Peso");
        list.add("CZK - Czech Koruna");
        list.add("VND - Vietnamese Dong");
        list.add("MAD - Moroccan Dirham");
        list.add("JOD - Jordanian Dinar");
        list.add("BHD - Bahraini Dinar");
        list.add("XOF - CFA Franc");
        list.add("LKR - Sri Lankan Rupee");
        list.add("UAH - Ukrainian Hryvnia");
        list.add("NGN - Nigerian Naira");
        list.add("TND - Tunisian Dinar");
        list.add("UGX - Ugandan Shilling");
        list.add("RON - Romanian New Leu");
        list.add("BDT - Bangladeshi Taka");
        list.add("PEN - Peruvian Nuevo Sol");
        list.add("GEL - Georgian Lari");
        list.add("XAF - Central African CFA Franc BEAC");
        list.add("FJD - Fijian Dollar");
        list.add("VEF - Venezuelan Bolivar");
        list.add("BYR - Belarusian Ruble");
        list.add("HRK - Croatian Kuna");
        list.add("UZS - Uzbekistani Som");
        list.add("BGN - Bulgarian Lev");
        list.add("DZD - Algerian Dinar");
        list.add("IRR - Iranian Rial");
        list.add("DOP - Dominican Peso");
        list.add("ISK - Icelandic Krona");
        list.add("XAG - Silver Ounce");
        list.add("CRC - Costa Rican Colon");
        list.add("SYP - Syrian Pound");
        list.add("LYD - Libyan Dinar");
        list.add("JMD - Jamaican Dollar");
        list.add("MUR - Mauritian Rupee");
        list.add("GHS - Ghanaian Cedi");
        list.add("AOA - Angolan Kwanza");
        list.add("UYU - Uruguayan Peso");
        list.add("AFN - Afghan Afghani");
        list.add("LBP - Lebanese Pound");
        list.add("XPF - CFP Franc");
        list.add("TTD - Trinidadian Dollar");
        list.add("TZS - Tanzanian Shilling");
        list.add("ALL - Albanian Lek");
        list.add("XCD - East Caribbean Dollar");
        list.add("GTQ - Guatemalan Quetzal");
        list.add("NPR - Nepalese Rupee");
        list.add("BOB - Bolivian Boliviano");
        list.add("ZWD - Zimbabwean Dollar");
        list.add("BBD - Barbadian or Bajan Dollar");
        list.add("CUC - Cuban Convertible Peso");
        list.add("LAK - Lao or Laotian Kip");
        list.add("BND - Bruneian Dollar");
        list.add("BWP - Botswana Pula");
        list.add("HNL - Honduran Lempira");
        list.add("PYG - Paraguayan Guarani");
        list.add("ETB - Ethiopian Birr");
        list.add("NAD - Namibian Dollar");
        list.add("PGK - Papua New Guinean Kina");
        list.add("SDG - Sudanese Pound");
        list.add("MOP - Macau Pataca");
        list.add("NIO - Nicaraguan Cordoba");
        list.add("BMD - Bermudian Dollar");
        list.add("KZT - Kazakhstani Tenge");
        list.add("PAB - Panamanian Balboa");
        list.add("BAM - Bosnian Convertible Marka");
        list.add("GYD - Guyanese Dollar");
        list.add("YER - Yemeni Rial");
        list.add("MGA - Malagasy Ariary");
        list.add("KYD - Caymanian Dollar");
        list.add("MZN - Mozambican Metical");
        list.add("RSD - Serbian Dinar");
        list.add("SCR - Seychellois Rupee");
        list.add("AMD - Armenian Dram");
        list.add("SBD - Solomon Islander Dollar");
        list.add("AZN - Azerbaijani New Manat");
        list.add("SLL - Sierra Leonean Leone");
        list.add("TOP - Tongan Pa'anga");
        list.add("BZD - Belizean Dollar");
        list.add("MWK - Malawian Kwacha");
        list.add("GMD - Gambian Dalasi");
        list.add("BIF - Burundian Franc");
        list.add("SOS - Somali Shilling");
        list.add("HTG - Haitian Gourde");
        list.add("GNF - Guinean Franc");
        list.add("MVR - Maldivian Rufiyaa");
        list.add("MNT - Mongolian Tughrik");
        list.add("CDF - Congolese Franc");
        list.add("STD - Sao Tomean Dobra");
        list.add("TJS - Tajikistani Somoni");
        list.add("KPW - North Korean Won");
        list.add("MMK - Burmese Kyat");
        list.add("LSL - Basotho Loti");
        list.add("LRD - Liberian Dollar");
        list.add("KGS - Kyrgyzstani Som");
        list.add("GIP - Gibraltar Pound");
        list.add("XPT - Platinum Ounce");
        list.add("MDL - Moldovan Leu");
        list.add("CUP - Cuban Peso");
        list.add("KHR - Cambodian Riel");
        list.add("MKD - Macedonian Denar");
        list.add("VUV - Ni-Vanuatu Vatu");
        list.add("MRO - Mauritanian Ouguiya");
        list.add("ANG - Dutch Guilder");
        list.add("SZL - Swazi Lilangeni");
        list.add("CVE - Cape Verdean Escudo");
        list.add("SRD - Surinamese Dollar");
        list.add("XPD - Palladium Ounce");
        list.add("SVC - Salvadoran Colon");
        list.add("BSD - Bahamian Dollar");
        list.add("XDR - IMF Special Drawing Rights");
        list.add("RWF - Rwandan Franc");
        list.add("AWG - Aruban or Dutch Guilder");
        list.add("DJF - Djiboutian Franc");
        list.add("BTN - Bhutanese Ngultrum");
        list.add("KMF - Comoran Franc");
        list.add("WST - Samoan Tala");
        list.add("SPL - Seborgan Luigino");
        list.add("ERN - Eritrean Nakfa");
        list.add("FKP - Falkland Island Pound");
        list.add("SHP - Saint Helenian Pound");
        list.add("JEP - Jersey Pound");
        list.add("TMT - Turkmenistani Manat");
        list.add("TVD - Tuvaluan Dollar");
        list.add("IMP - Isle of Man Pound");
        list.add("GGP - Guernsey Pound");
        list.add("ZMW - Zambian Kwacha");
        return list;
    }

}
