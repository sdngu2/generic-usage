package com.nc5xb3.genericusage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nc5xb3.genericusage.controller.impl.SourceController;
import com.nc5xb3.genericusage.controller.impl.SourceTemplateController;
import com.nc5xb3.genericusage.model.source.Record;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.view.SourceLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EditSourceActivity extends AppCompatActivity {

    public static String EXTRA_IS_ADDED = "added";
    public static String EXTRA_TEMPLATE_ID = "templateId";
    public static String EXTRA_SOURCE_ID = "sourceId";

    private SourceController sourceController;
    private SourceLayoutManager sourceLayoutManager;

    private Template template;
    private Source source;

    private String templateId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_source);

        SourceTemplateController sourceTemplateController = SourceTemplateController.getInstance();
        sourceController = SourceController.getInstance();

        sourceLayoutManager = new SourceLayoutManager();

        Intent intent = getIntent();
        boolean isAdded = intent.getBooleanExtra(EXTRA_IS_ADDED, false);
        String sourceId = intent.getStringExtra(EXTRA_SOURCE_ID);
        templateId = intent.getStringExtra(EXTRA_TEMPLATE_ID);

        //FIXME Find proper way of dealing this
        if (isAdded) {
            // If there's no sourceId specified should usually is, then generate source id
            if (sourceId == null || sourceId.isEmpty()) {
                do {
                    sourceId = UUID.randomUUID().toString();
                } while (sourceController.contains(sourceId));
                source = new Source(sourceId);
                if (templateId == null || templateId.isEmpty()) {
                    // ERROR!
                } else if (sourceTemplateController.get(templateId) == null) {
                    // ERROR!
                } else {
                    source.setTemplateId(templateId);
                }
            }
        } else {
            if (sourceId == null || sourceId.isEmpty()) {
                // ERROR!
            } else if ((source = sourceController.get(sourceId)) == null) {
                // ERROR!
            } else {
                templateId = source.getTemplateId();
            }
        }
        template = sourceTemplateController.get(templateId);
        String title = source.getTitle();
        if (isAdded) {
            title = generateUniqueName(template.getName());
        }

        // Set layout
        setContentView(sourceLayoutManager.getEditLayout(template, this, title));

        // Set actionbar title and data
        if (isAdded) {
            setTitle("New " + template.getName());
        } else {
            setTitle("Edit " + template.getName());
            try {
                sourceLayoutManager.setSourceData(source);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private String generateUniqueName(String name) {
        // Make unique title name for source
        List<Source> sourceList = sourceController.getAll();
        String title = name;
        int uniqueNum = 0;
        boolean titleExists;
        do {
            titleExists = false;
            for (Source source : sourceList) {
                if (source.getTitle().equals(title)) {
                    titleExists = true;
                    break;
                }
            }
            if (titleExists) {
                title = name + " (" + ++uniqueNum + ")";
            }
        } while (titleExists);
        return title;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_source, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveSource();
            setResult(RESULT_OK, new Intent());
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveSource() {
        source.setTemplateId(templateId);
        source.setTitle(sourceLayoutManager.getTitle());
        try {
            source.setUserInput(sourceLayoutManager.getViewMap());
        } catch (Exception e) {
            e.printStackTrace();
        }
        source.setRecordList(new ArrayList<Record>());
        sourceController.upsert(source);
    }
}
