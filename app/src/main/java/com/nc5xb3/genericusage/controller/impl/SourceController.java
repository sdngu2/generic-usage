package com.nc5xb3.genericusage.controller.impl;

import com.nc5xb3.genericusage.controller.InterfaceController;
import com.nc5xb3.genericusage.manager.impl.SourceManager;
import com.nc5xb3.genericusage.model.source.Source;

import java.util.List;

/**
 * Created by Steven Nguyen on 12/1/2015.
 */
public class SourceController implements InterfaceController<Source> {

    private static SourceController instance = null;
    public static synchronized SourceController getInstance() {
        return instance = instance != null ? instance : new SourceController();
    }


    private SourceManager sourceManager;

    public SourceController() {
        sourceManager = new SourceManager();
    }

    @Override
    public List<Source> getAll() {
        return sourceManager.getAll();
    }

    @Override
    public Source get(String id) {
        return sourceManager.get(id);
    }

    @Override
    public Source upsert(Source object) {
        return sourceManager.upsert(object);
    }

    @Override
    public Source remove(String id) {
        return sourceManager.remove(id);
    }

    @Override
    public boolean contains(String id) {
        return sourceManager.contains(id);
    }

}
