package com.nc5xb3.genericusage.controller;

import java.util.List;

/**
 * Created by Steven Nguyen on 12/4/2015.
 */
public interface InterfaceController<S> {

    List<S> getAll();

    S get(String id);

    S upsert(S object);

    S remove(String id);

    boolean contains(String id);

}
