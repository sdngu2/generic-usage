package com.nc5xb3.genericusage.controller.impl;

import com.nc5xb3.genericusage.controller.InterfaceController;
import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.manager.impl.TemplateManager;

import java.util.List;

/**
 * Created by Steven Nguyen on 11/29/2015.
 */
public class SourceTemplateController implements InterfaceController<Template> {

    private static SourceTemplateController instance = null;
    public static synchronized SourceTemplateController getInstance() {
        return instance = instance != null ? instance : new SourceTemplateController();
    }

    private TemplateManager templateManager;

    public SourceTemplateController() {
        templateManager = new TemplateManager();
    }

    @Override
    public List<Template> getAll() {
        return templateManager.getAll();
    }

    @Override
    public Template get(String id) {
        return templateManager.get(id);
    }

    @Override
    public Template upsert(Template object) {
        return templateManager.upsert(object);
    }

    @Override
    public Template remove(String id) {
        return templateManager.remove(id);
    }

    @Override
    public boolean contains(String id) {
        return templateManager.contains(id);
    }

}
