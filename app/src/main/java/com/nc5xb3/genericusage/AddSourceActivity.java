package com.nc5xb3.genericusage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.app.SearchManager;
import android.support.v7.widget.SearchView;

import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.view.adapter.TemplateAdapter;
import com.nc5xb3.genericusage.controller.impl.SourceTemplateController;

public class AddSourceActivity extends AppCompatActivity implements
        ListView.OnItemClickListener, SearchView.OnQueryTextListener {

    private static final int EDIT_NEW_SOURCE_REQUEST = 1;

    private SourceTemplateController sourceTemplateController;

    private ListView listViewTemplates;
    private TemplateAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_source);

        sourceTemplateController = SourceTemplateController.getInstance();

        adapter = new TemplateAdapter(this, sourceTemplateController.getAll());

        listViewTemplates = (ListView) findViewById(R.id.listViewAvailableSources);
        listViewTemplates.setOnItemClickListener(this);

        listViewTemplates.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_source, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EDIT_NEW_SOURCE_REQUEST) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, EditSourceActivity.class);

        String templateId =
                ((Template) listViewTemplates.getItemAtPosition(i)).getTemplateId();

        intent.putExtra(EditSourceActivity.EXTRA_IS_ADDED, true);
        intent.putExtra(EditSourceActivity.EXTRA_TEMPLATE_ID, templateId);

        setResult(RESULT_OK, intent);
        startActivityForResult(intent, EDIT_NEW_SOURCE_REQUEST);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}
