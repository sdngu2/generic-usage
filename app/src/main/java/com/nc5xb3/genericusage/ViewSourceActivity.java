package com.nc5xb3.genericusage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nc5xb3.genericusage.controller.impl.SourceController;
import com.nc5xb3.genericusage.controller.impl.SourceTemplateController;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.view.SourceLayoutManager;

public class ViewSourceActivity extends AppCompatActivity {

    public static String EXTRA_SOURCE_ID = "sourceId";

    private SourceController sourceController;
    private SourceLayoutManager sourceLayoutManager;

    private Source source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_source);

        SourceTemplateController sourceTemplateController = SourceTemplateController.getInstance();
        sourceController = SourceController.getInstance();

        sourceLayoutManager = new SourceLayoutManager();

        Intent intent = getIntent();
        String sourceId = intent.getStringExtra(EXTRA_SOURCE_ID);

        source = sourceController.get(sourceId);

        if (source != null) {
            Template template = sourceTemplateController.get(source.getTemplateId());

            // Set layout
            setContentView(sourceLayoutManager.getViewLayout(template, this));
        } else {
            // Error!
        }

    }
}
