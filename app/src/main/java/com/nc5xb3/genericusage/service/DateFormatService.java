package com.nc5xb3.genericusage.service;

import java.util.Date;

/**
 * Created by Steven Nguyen on 12/6/2015.
 */
public class DateFormatService {

    public static String getLastUpdate(Date date) {
        String message = "Updated ";
        Date currentDate = new Date();
        long timeDiff = currentDate.getTime() - date.getTime();

        long diffDays = timeDiff / (24 * 60 * 60 * 1000);
        if (diffDays == 1) {
            message += "yesterday";
        } else if (diffDays > 1) {
            message += diffDays + " days ago";
        } else {
            long diffHours = timeDiff / (60 * 60 * 1000) % 24;
            if (diffHours == 1) {
                message += "an hour ago";
            } else if (diffHours > 1) {
                message += diffHours + " hours ago";
            } else {
                long diffMinutes = timeDiff / (60 * 1000) % 60;
                if (diffMinutes == 1) {
                    message += "a minute ago";
                } else if (diffMinutes > 1) {
                    message += diffMinutes + " minutes ago";
                } else {
                    long diffSeconds = timeDiff / 1000 % 60;
                    if (diffSeconds == 1) {
                        message += "a second ago";
                    } else if (diffSeconds > 1) {
                        message += diffSeconds + " seconds ago";
                    } else {
                        message = "Just Updated";
                    }
                }
            }
        }

        return message;
    }
}
