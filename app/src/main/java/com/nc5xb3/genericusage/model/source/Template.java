package com.nc5xb3.genericusage.model.source;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Steven Nguyen on 11/26/2015.
 */
public class Template {

    private String templateId;
    private String version;
    private Date lastUpdated;

    private Bitmap image;
    private String imageUrl;
    private String name;
    private String description;
    private List<String> categories;

    private List<Input> formInputList;

    public Template(String templateId) {
        this.templateId = templateId;
        version = "0";
        lastUpdated = null;
        image = null;
        imageUrl = "";
        name = "";
        description = "";
        categories = new ArrayList<>();
        formInputList = new ArrayList<>();
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<Input> getFormInputList() {
        return formInputList;
    }

    public void setFormInputList(List<Input> formInputList) {
        this.formInputList = formInputList;
    }

}
