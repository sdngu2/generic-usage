package com.nc5xb3.genericusage.model.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Steven Nguyen on 11/27/2015.
 */
public class Source {

    private String sourceId;
    private String templateId; // References Template
    private String title;
    private Date dateCreated;
    private Map<String, String> userInput;
    private List<Record> recordList;

    public Source(String sourceId) {
        this.sourceId = sourceId;
        templateId = "";
        title = "";
        dateCreated = new Date();
        userInput = new HashMap<>();
        recordList = new ArrayList<>();
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Map<String, String> getUserInput() {
        return userInput;
    }

    public void setUserInput(Map<String, String> userInput) {
        this.userInput = userInput;
    }

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }
}
