package com.nc5xb3.genericusage.model.source;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven Nguyen on 11/27/2015.
 */
public class Input {

    private String name;
    private String label;
    private String type;
    private List<String> values;
    private String hint;

    public Input() {
        name = "";
        label = "";
        type = "";
        values = new ArrayList<>();
        hint = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }
}
