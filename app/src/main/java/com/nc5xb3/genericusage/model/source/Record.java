package com.nc5xb3.genericusage.model.source;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by Steven Nguyen on 11/28/2015.
 */
public class Record implements Comparable {

    private HashMap<String, String> data;
    private Date date;

    public Record() {
        data = new HashMap<>();
        date = new Date();
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(Object o) {
        Record other = (Record) o;
        return other.getDate().compareTo(date);
    }
}
