package com.nc5xb3.genericusage.manager.impl;

import com.nc5xb3.genericusage.manager.InterfaceManager;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.model.source.Template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Steven Nguyen on 11/29/2015.
 */
public class TemplateManager implements InterfaceManager<Template> {

    private Map<String, Template> templates;

    public TemplateManager() {
        this.templates = new HashMap<>();
    }

    @Override
    public List<Template> getAll() {
        return new ArrayList<>(templates.values());
    }

    @Override
    public Template get(String id) {
        return templates.get(id);
    }

    @Override
    public Template upsert(Template object) {
        return templates.put(object.getTemplateId(), object);
    }

    @Override
    public Template remove(String id) {
        return templates.remove(id);
    }

    @Override
    public boolean contains(String id) {
        return templates.containsKey(id);
    }

}
