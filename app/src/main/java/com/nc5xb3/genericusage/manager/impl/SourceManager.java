package com.nc5xb3.genericusage.manager.impl;

import com.nc5xb3.genericusage.manager.InterfaceManager;
import com.nc5xb3.genericusage.model.source.Source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Steven Nguyen on 11/29/2015.
 */
public class SourceManager implements InterfaceManager<Source> {

    private Map<String, Source> sources;

    public SourceManager() {
        this.sources = new HashMap<>();
    }

    @Override
    public List<Source> getAll() {
        return new ArrayList<>(sources.values());
    }

    @Override
    public Source get(String id) {
        return sources.get(id);
    }

    @Override
    public Source upsert(Source object) {
        return sources.put(object.getSourceId(), object);
    }

    @Override
    public Source remove(String id) {
        return sources.remove(id);
    }

    @Override
    public boolean contains(String id) {
        return sources.containsKey(id);
    }

}
