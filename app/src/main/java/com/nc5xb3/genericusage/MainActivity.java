package com.nc5xb3.genericusage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.nc5xb3.genericusage.controller.impl.SourceController;
import com.nc5xb3.genericusage.controller.impl.SourceTemplateController;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.predefined.PredefinedTemplates;
import com.nc5xb3.genericusage.view.adapter.SourceAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private static int ADD_SOURCE_REQUEST = 1;
    private static int VIEW_SOURCE_REQUEST = 2;
    private static int EDIT_SOURCE_REQUEST = 3;

    private SourceController sourceController;

    private String focusedSourceId;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView sourceListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialise Controller
        sourceController = SourceController.getInstance();

        SourceTemplateController sourceTemplateController = SourceTemplateController.getInstance();
        //TODO Remove
        sourceTemplateController.upsert(PredefinedTemplates.getExample());
        for (int i = 0; i <= 10; i++) {
            sourceTemplateController.upsert(PredefinedTemplates.getXE(String.valueOf(i), this));
        }

        // Get views
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutMain);
        sourceListView = (ListView) findViewById(R.id.listViewCurrentSources);

        registerForContextMenu(sourceListView);
        sourceListView.setOnItemClickListener(this);
        sourceListView.setOnItemLongClickListener(this);

        refreshListView();
    }

    private void refreshListView() {
        List<Source> sourceList = sourceController.getAll();
        Collections.sort(sourceList, new Comparator<Source>() {
            @Override
            public int compare(Source s1, Source s2) {
                return s1.getTitle().compareTo(s2.getTitle());
            }
        });
        sourceListView.setAdapter(new SourceAdapter(this, sourceList));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addSource();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        focusedSourceId = ((Source)sourceListView.getItemAtPosition(i)).getSourceId();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        focusedSourceId = ((Source)sourceListView.getItemAtPosition(i)).getSourceId();
        return sourceListView.showContextMenu();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshSource(focusedSourceId);
                return true;
            case R.id.action_edit:
                editSource(focusedSourceId);
                return true;
            case R.id.action_delete:
                deleteSource(focusedSourceId);
                refreshListView();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void addSource() {
        Intent intent = new Intent(this, AddSourceActivity.class);
        startActivityForResult(intent, ADD_SOURCE_REQUEST);
    }

    private void refreshSource(String sourceId) {
        String message = sourceController.get(sourceId).getTitle();
        message += " is refreshing!";
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void editSource(String sourceId) {
        Intent intent = new Intent(this, EditSourceActivity.class);
        intent.putExtra(EditSourceActivity.EXTRA_SOURCE_ID, sourceId);
        startActivityForResult(intent, EDIT_SOURCE_REQUEST);
    }

    private void deleteSource(final String sourceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_delete_source)
                .setPositiveButton(R.string.dialog_confirm_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (sourceController.contains(sourceId)) {
                            sourceController.remove(sourceId);
                            refreshListView();
                        } else {
                            Toast.makeText(getApplicationContext(), "Source was not found!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_confirm_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "NO", Toast.LENGTH_SHORT).show();
                    }
                }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if  (resultCode == RESULT_OK) {
            if (requestCode == ADD_SOURCE_REQUEST) {
                refreshListView();
            } else if (requestCode == VIEW_SOURCE_REQUEST) {

            } else if (requestCode == EDIT_SOURCE_REQUEST) {

            }
        }
    }

}
