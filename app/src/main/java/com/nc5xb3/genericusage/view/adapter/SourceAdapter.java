package com.nc5xb3.genericusage.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nc5xb3.genericusage.R;
import com.nc5xb3.genericusage.controller.impl.SourceTemplateController;
import com.nc5xb3.genericusage.model.source.Record;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.service.DateFormatService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Steven Nguyen on 12/6/2015.
 */
public class SourceAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Source> sourceList;
    private List<Source> sourceListFiltered;

    private SourceTemplateController sourceTemplateController;

    public SourceAdapter(Context context, List<Source> sourceList) {
        this.context = context;
        this.sourceList = sourceList;
        this.sourceListFiltered = sourceList;

        Collections.sort(sourceList, new Comparator<Source>() {
            @Override
            public int compare(Source s1, Source s2) {
                return s1.getTitle().compareTo(s2.getTitle());
            }
        });

        sourceTemplateController = SourceTemplateController.getInstance();
    }

    @Override
    public int getCount() {
        return sourceListFiltered.size();
    }

    @Override
    public Object getItem(int i) {
        return sourceListFiltered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if view contains been created for row, else inflate
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_source, null); // List layout here
        }

        if (!sourceListFiltered.isEmpty()) {
            ImageView sourceImage = (ImageView) view.findViewById(R.id.imageViewSourceLogo);
            TextView sourceTitle = (TextView) view.findViewById(R.id.textViewSourceTitle);
            TextView sourceDescription = (TextView) view.findViewById(R.id.textViewSourceDescription);
            TextView sourceData = (TextView) view.findViewById(R.id.textViewSourceData);
            TextView sourceLastUpdated = (TextView) view.findViewById(R.id.textViewSourceLastUpdated);

            Source source = sourceListFiltered.get(i);
            List<Record> records = source.getRecordList();

            sourceTitle.setText(source.getTitle());
            sourceDescription.setText("");


            if (records == null || records.size() == 0) {
                sourceData.setText("No data");
                sourceLastUpdated.setText("Never");
            } else {
                sourceData.setText("1234");
                sourceLastUpdated.setText(DateFormatService.getLastUpdate(records.get(0).getDate()));
            }

            String templateId = source.getTemplateId();
            Template template = sourceTemplateController.get(templateId);
            if (template.getImage() != null && template.getImage().getByteCount() > 1) {
                sourceImage.setImageBitmap(template.getImage());
            } else {
                sourceImage.setImageResource(R.drawable.abc_ic_clear_mtrl_alpha);
            }
        }

        return view;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}
