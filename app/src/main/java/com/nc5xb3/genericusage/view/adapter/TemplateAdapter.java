package com.nc5xb3.genericusage.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nc5xb3.genericusage.R;
import com.nc5xb3.genericusage.model.source.Template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Steven Nguyen on 11/29/2015.
 */
public class TemplateAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Template> templateList;
    private List<Template> templateListFiltered;

    public TemplateAdapter(Context context, List<Template> templateList) {
        this.context = context;
        this.templateList = templateList;
        this.templateListFiltered = templateList;

        Collections.sort(templateList, new Comparator<Template>() {
            @Override
            public int compare(Template t1, Template t2) {
                return t1.getName().compareTo(t2.getName());
            }
        });
    }

    @Override
    public int getCount() {
        return templateListFiltered.size();
    }

    @Override
    public Object getItem(int i) {
        return templateListFiltered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if view contains been created for row, else inflate
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_available_source, null); // List layout here
        }

        if (!templateListFiltered.isEmpty()) {
            ImageView sourceImage = (ImageView) view.findViewById(R.id.imageViewAvailableSourceLogo);
            TextView sourceTitle = (TextView) view.findViewById(R.id.textViewAvailableSourceName);
            TextView sourceDescription = (TextView) view.findViewById(R.id.textViewAvailableSourceDescription);
            TextView sourceVersion = (TextView) view.findViewById(R.id.textViewAvailableSourceVersion);
            TextView sourceStatus = (TextView) view.findViewById(R.id.textViewAvailableSourceStatus);
            TextView sourceLastUpdated = (TextView) view.findViewById(R.id.textViewAvailableSourceLastUpdated);

            Template source = templateListFiltered.get(i);

            sourceTitle.setText(source.getName());
            sourceDescription.setText(source.getDescription());
            sourceVersion.setText(source.getVersion());
            sourceStatus.setText("Working");
            sourceLastUpdated.setText("10/07/2015");
            if (source.getImage() != null && source.getImage().getByteCount() > 1) {
                sourceImage.setImageBitmap(source.getImage());
            } else {
                sourceImage.setImageResource(R.drawable.abc_ic_clear_mtrl_alpha);
            }
        }

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                List<Template> filteredList = new ArrayList<>();

                if (charSequence.length() == 0) {
                    filteredList = templateList;
                } else {
                    for (Template template : templateList) {
                        String searchableText =
                                (template.getName() + template.getDescription()).toLowerCase();
                        if (searchableText.contains(charSequence)) {
                            filteredList.add(template);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                templateListFiltered = (List<Template>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
