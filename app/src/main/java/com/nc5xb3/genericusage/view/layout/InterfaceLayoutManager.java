package com.nc5xb3.genericusage.view.layout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Steven Nguyen on 12/4/2015.
 */
public interface InterfaceLayoutManager {

    ViewGroup.LayoutParams getLayoutParamsWidth(int left, int top, int right, int bottom);
    void setLayoutPadding(int padding);
    View getLayout();
    TextView addTextView(String text);
    EditText addEditText(String text, String hint);
    AutoCompleteTextView addAutoCompleteTextView(
            String text, String hint, final List<String> list);
}
