package com.nc5xb3.genericusage.view;

import android.content.Context;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AbsSpinner;
import android.widget.EditText;
import android.widget.TextView;

import com.nc5xb3.genericusage.model.source.Input;
import com.nc5xb3.genericusage.model.source.Source;
import com.nc5xb3.genericusage.model.source.Template;
import com.nc5xb3.genericusage.view.layout.InterfaceLayoutManager;
import com.nc5xb3.genericusage.view.layout.impl.LinearLayoutManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Steven Nguyen on 12/1/2015.
 */
public class SourceLayoutManager {

    public final static String TYPE_TEXT = "text";
    public final static String TYPE_PASSWORD = "password";
    public final static String TYPE_NUMBER = "number";

    private Map<String, View> views;
    private TextView titleView;

    public SourceLayoutManager() {
        views = new HashMap<>();
    }

    public View getEditLayout(Template template, Context context) {
        return getEditLayout(template, context, template.getName());
    }

    public View getEditLayout(Template template, Context context, String titleName) {
        InterfaceLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL);
        layoutManager.setLayoutPadding(32);

        titleView = applyStyleTitle(layoutManager.addEditText(titleName, "Title"));

        for (Input input : template.getFormInputList()) {
            if (!input.getLabel().isEmpty()) {
                layoutManager.addTextView(input.getLabel());
            }
            switch (input.getType()) {
                case TYPE_TEXT:
                    if (input.getValues().size() == 1) {
                        views.put(input.getName(),
                                layoutManager.addEditText(
                                        input.getValues().get(0), input.getHint()
                                )
                        );
                    } else {
                        views.put(input.getName(),
                                layoutManager.addAutoCompleteTextView(
                                        input.getValues().get(0), input.getHint(),
                                        input.getValues().subList(1, input.getValues().size())
                                )
                        );
                    }
                    break;
                case TYPE_PASSWORD:
                    views.put(input.getName(),
                            applyStylePassword(
                                    layoutManager.addEditText(
                                            input.getValues().get(0), input.getHint()
                                    )
                            )
                    );
                    break;
                case TYPE_NUMBER:
                    views.put(input.getName(),
                            applyStyleNumber(
                                    layoutManager.addEditText(
                                            input.getValues().get(0), input.getHint()
                                    )
                            )
                    );
                    break;
                default:
                    layoutManager.addTextView("Unknown type: " + input.getType());
                    break;
            }
        }

        return layoutManager.getLayout();
    }

    public View getViewLayout(Template template, Context context) {
        // TODO Replace this
        return getEditLayout(template, context);
    }

    public String getTitle() {
        return String.valueOf(titleView.getText());
    }

    public void setSourceData(Source source) throws Exception {
        // TODO read values of source and update views here
        for (String key : source.getUserInput().keySet()) {
            setViewValue(key, source.getUserInput().get(key));
        }
    }

    public Map<String, String> getViewMap() throws Exception {
        Map<String, String> values = new HashMap<>();
        for (String key : views.keySet()) {
            values.put(key, getViewValue(key));
        }
        return values;
    }

    private void setViewValue(String key, String value) throws Exception {
        if (views.containsKey(key)) {
            View view = views.get(key);
            if (view instanceof TextView) {
                ((TextView) view).setText(value);
            } else if (view instanceof AbsSpinner) {
                AbsSpinner spinner = (AbsSpinner) view;
                for (int i = 0; i < spinner.getCount(); i++) {
                    if (spinner.getItemAtPosition(i).equals(value)) {
                        spinner.setSelection(i);
                        break;
                    }
                }
            } else {
                throw new Exception("Unknown view type: " + view.getClass().getName());
            }
        }
    }

    private String getViewValue(String name) throws Exception {
        View view = views.get(name);
        if (view instanceof TextView) {
            return String.valueOf(((TextView) view).getText());
        } else if (view instanceof AbsSpinner) {
            return ((AbsSpinner) view).getSelectedItem().toString();
        } else {
            throw new Exception("Unknown view type: " + view.getClass().getName());
        }
    }

    // STYLES

    private EditText applyStyleTitle(EditText editText) {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        editText.setTextSize(32f);
        return editText;
    }

    private EditText applyStylePassword(EditText editText) {
        editText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        return editText;
    }

    private EditText applyStyleNumber(EditText editText) {
        editText.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        return editText;
    }

}
