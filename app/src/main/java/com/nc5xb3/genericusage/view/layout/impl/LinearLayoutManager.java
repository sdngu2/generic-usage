package com.nc5xb3.genericusage.view.layout.impl;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nc5xb3.genericusage.view.layout.InterfaceLayoutManager;

import java.util.List;

/**
 * Created by Steven Nguyen on 11/28/2015.
 */
public class LinearLayoutManager implements InterfaceLayoutManager {

    protected Context context;
    protected LinearLayout layout;

    public final static int HORIZONTAL = LinearLayout.HORIZONTAL;
    public final static int VERTICAL = LinearLayout.VERTICAL;

    public LinearLayoutManager(Context context, int orientation) {
        this.context = context;
        layout = new LinearLayout(this.context);
        layout.setOrientation(orientation);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
    }

    public ViewGroup.LayoutParams getLayoutParamsWidth(int left, int top, int right, int bottom) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(left, top, right, bottom);
        return layoutParams;
    }

    public void setLayoutPadding(int padding) {
        layout.setPadding(padding, padding, padding, padding);
    }

    public View getLayout() {
        return layout;
    }

    public TextView addTextView(String text) {
        TextView textView = new TextView(context);
        textView.setLayoutParams(getLayoutParamsWidth(0, 0, 0, 0));
        textView.setText(text);
        textView.setTextSize(20f);

        layout.addView(textView);
        return textView;
    }

    public EditText addEditText(String text, String hint) {
        EditText editText = new EditText(context);
        editText.setLayoutParams(getLayoutParamsWidth(0, 0, 0, 30));
        editText.setText(text);
        editText.setHint(hint);

        layout.addView(editText);
        return editText;
    }

    public AutoCompleteTextView addAutoCompleteTextView(
            String text, String hint, final List<String> list) {
        AutoCompleteTextView textView = new AutoCompleteTextView(context);
        textView.setLayoutParams(getLayoutParamsWidth(0, 0, 0, 30));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context,
                android.R.layout.simple_list_item_1,
                list);
        textView.setAdapter(adapter);
        textView.setValidator(new AutoCompleteTextView.Validator() {
            @Override
            public boolean isValid(CharSequence charSequence) {
                return list.contains(charSequence.toString());
            }

            @Override
            public CharSequence fixText(CharSequence charSequence) {
                String input = charSequence.toString().toLowerCase();
                for (String correctString : list) {
                    if (correctString.toLowerCase().contains(input)) {
                        return correctString;
                    }
                }
                return "";
            }
        });
        textView.setThreshold(1);
        textView.setSelectAllOnFocus(true);
        textView.setText(text);
        textView.setHint(hint);
        textView.setSingleLine();
        textView.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        layout.addView(textView);
        return textView;
    }

}
